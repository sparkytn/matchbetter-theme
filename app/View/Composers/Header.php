<?php

namespace App\View\Composers;

use Roots\Acorn\View\Composer;
use App\Helpers\NavWalkerBS5;

/**
 * Class Header
 * @package App\View\Composers
 */
class Header extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'sections.header',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'primaryMenuArgs' => $this->primaryMenuArgs(),
        ];
    }

    /**
     * Return Primary Menu
     *
     * @return array
     */
    public function primaryMenuArgs()
    {
        $args = array(
            'theme_location'    => 'primary_navigation',
            'menu_class'        => 'navbar-nav',
            'walker'            => new NavWalkerBS5(),
        );
        return $args;
    }
}

