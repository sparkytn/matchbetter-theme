import {domReady} from '@roots/sage/client';
import $ from 'jquery';
import 'bootstrap';
import Swiper, {Autoplay, Navigation, Pagination, EffectFade, Lazy} from 'swiper';

/**
 * app.main
 */
const main = async (err) => {
  if (err) {
    // handle hmr errors
    console.error(err);
  }

  Swiper.use([Navigation, Pagination, Autoplay, EffectFade, Lazy]);
  // application code
  $('.action-slider').each(function() {
    let $this = $(this);
    new Swiper(this, {
      // Default parameters
      slidesPerView: 1,
      spaceBetween: 30,
      pauseOnMouseEnter: true,
      speed: 600,
      loop: true,
      autoplay: {
        delay: 3000,
        // disableOnInteraction: true,
      },
      navigation: {
        nextEl: $this.find('.swiper-button-next')[0],
        prevEl: $this.find('.swiper-button-prev')[0],
      },

      breakpoints: {
        // when window width is >= 320px
        320: {
          slidesPerView: 1,
        },
        // when window width is >= 480px
        640: {
          slidesPerView: 2,
        },

        1000: {
          slidesPerView: 3,
        },

        // when window width is >= 640px

      }
    });
  });
};

/**
 * Initialize
 *
 * @see https://webpack.js.org/api/hot-module-replacement
 */
domReady(main);
import.meta.webpackHot?.accept(main);
