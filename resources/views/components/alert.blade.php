<div {{ $attributes->merge(['class' => $type]) }}>
  <h4 class="mb-0 text-tertiary">{!! $message ?? $slot !!}</h4>
</div>
