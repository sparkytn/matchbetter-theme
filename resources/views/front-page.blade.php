@extends('layouts.app')

@section('content')
<section class="bg-dark">
    <div class="container py-5">
        <div class="row g-4">
            <!-- Left big card -->

            @php
                $featuredQuery = new WP_Query([
		            'post_type' => 'post',
                    'posts_per_page' => 3,
                    'meta_query' => [
                        [
                            'key' => 'featured',
                            'value' => 1
                        ]
                    ]
                ]);
                $index = 0;
                $featuredPosts = [];
                if ( $featuredQuery->have_posts() ) {
                    while ( $featuredQuery->have_posts() ) {
                        $post = $featuredQuery->the_post();

                        $featuredPosts[] = get_the_ID();
            @endphp
            @if($index === 0)

                <div class="col-lg-6">
                    <div class="card card-overlay-bottom card-grid-lg card-bg-scale" style="background-image:url({{ get_the_post_thumbnail_url(get_the_ID()) }}); background-position: center left; background-size: cover;">
                        <!-- Card Image overlay -->
                        <div class="card-img-overlay d-flex align-items-center p-3 p-sm-4">
                            <div class="w-100 mt-auto">
                                <!-- Card category -->
                                {!! get_the_category_list() !!}
                            <!-- Card title -->
                                <h2 class="text-white h1"><a href="@permalink" class="btn-link stretched-link text-reset">{!! get_the_title() !!}</a></h2>
                                <p class="text-white">{!! get_the_excerpt() !!}</p>
                                <!-- Card info -->
                                <small class="nav-item text-white opacity-50">{!! get_the_date() !!}</small>
                            </div>
                        </div>
                    </div>
                </div>
            @elseif($index === 1)
            <!-- Right small cards -->
                <div class="col-lg-6">
                    <div class="row g-4">
                        <!-- Card item START -->
                        <div class="col-12">
                            <div class="card card-overlay-bottom card-grid-sm card-bg-scale" style="background-image:url({{ get_the_post_thumbnail_url(get_the_ID()) }}); background-position: center left; background-size: cover;">
                                <!-- Card Image -->
                                <!-- Card Image overlay -->
                                <div class="card-img-overlay d-flex align-items-center p-3 p-sm-4">
                                    <div class="w-100 mt-auto">
                                        <!-- Card category -->
                                        {!! get_the_category_list() !!}
                                    <!-- Card title -->
                                        <h4 class="text-white"><a href="@permalink" class="btn-link stretched-link text-reset">{!! get_the_title() !!}</a></h4>
                                        <!-- Card info -->
                                        <small class="nav-item text-white opacity-50">{!! get_the_date() !!}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Card item END -->
            @else
                        <!-- Card item START -->
                            <div class="col-12">
                                <div class="card card-overlay-bottom card-grid-sm card-bg-scale" style="background-image:url({{ get_the_post_thumbnail_url(get_the_ID()) }}); background-position: center left; background-size: cover;">
                                    <!-- Card Image overlay -->
                                    <div class="card-img-overlay d-flex align-items-center p-3 p-sm-4">
                                        <div class="w-100 mt-auto">
                                            <!-- Card category -->
                                            {!! get_the_category_list() !!}
                                        <!-- Card title -->
                                            <h4 class="text-white"><a href="@permalink" class="btn-link stretched-link text-reset">{!! get_the_title() !!}</a></h4>
                                            <!-- Card info -->
                                            <ul class="nav nav-divider text-white align-items-center d-none d-sm-inline-block">
                                                <li class="nav-item">{!!  get_the_date() !!}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Card item END -->

                    </div>
                </div>
            @endif
            @php($index++)
        <?php
}
}

// Restore original Post Data
wp_reset_postdata();
?>
        </div>
    </div>
</section>
    <!-- Become do Section -->
    <section class="section">
        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <div class="mb-5">
                        <h2 class="text-tertiary">آخر الأخبار</h2>
                    </div>
                    <?php
                    $postsQuery = new WP_Query([
                        'post_type' => 'post',
                        'posts_per_page' => 15,
                        'post__not_in' => $featuredPosts,
                    ]);
                    ?>
                    <div class="row">




                        @posts($postsQuery)
                        <div class="col-lg-4">
                            @include('partials.content')
                        </div>
                        @endposts
                    </div>
                </div>
{{--                <div class="col-lg-4"></div>--}}
            </div>



        </div>
    </section>
    <!-- /* END become do Section -->

@endsection

{{--@section('sidebar')--}}
{{--  @include('sections.sidebar')--}}
{{--@endsection--}}
