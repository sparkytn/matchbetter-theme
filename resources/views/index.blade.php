@extends('layouts.app')

@section('page-header')
    @include('partials.page-header')
@endsection

@section('content')



    @if (! have_posts())
        <x-alert type="warning" class=" p-3 rounded bg-light mb-4 ">
            عذرا، لم يتم العثور على نتائج
        </x-alert>
        <div class="w-50 mt-5">
            {!! get_search_form(false) !!}
        </div>

    @endif
    <div class="row">
    @while(have_posts()) @php(the_post())
        <div class="col-lg-4">
            @includeFirst(['partials.content-' . get_post_type(), 'partials.content'])
        </div>
    @endwhile
    </div>
    <div class="mb-4 text-center">
        {!! get_the_posts_pagination([
		    'prev_text'          => '<i class="fas fa-chevron-left"></i>',
            'next_text'          => '<i class="fas fa-chevron-right"></i>',
        ]) !!}
    </div>


@endsection

@section('sidebar')
    @include('sections.sidebar')
@endsection
