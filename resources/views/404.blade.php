@extends('layouts.app')

@section('page-header')
    @include('partials.page-header')
@endsection

@section('content')
  <div class="container">

  @if (! have_posts())
    <x-alert type="warning" class="mb-5">
      {!! __('معذرة ، الصفحة التي تحاول عرضها غير موجودة', 'sage') !!}
    </x-alert>

      <div class="w-50 mt-5">
          {!! get_search_form(false) !!}
      </div>
  @endif
  </div>
@endsection

@section('sidebar')
    @include('sections.sidebar')
@endsection
