
    <div class="row justify-content-center">
        <div class="col-lg-9">
            <article @php(post_class('py-5'))>
            <header class="mb-4">
                <?php
                    if ( has_post_thumbnail() ) {
                        the_post_thumbnail( [ 1024, 450] , ['class'=>'img-fluid mb-3 rounded']);
                    }
                ?>
                <h1 class="entry-title text-primary">
                    {!! $title !!}
                </h1>

                @include('partials.entry-meta')
            </header>

            <div class="entry-content">
                @php(the_content())
            </div>

            <footer>
                {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav text-center"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
            </footer>

{{--            @php(comments_template())--}}
            </article>
        </div>
    </div>


