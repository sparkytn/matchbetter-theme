<article @php(post_class())>
    <div class="card mb-4">
        <div class="position-relative">
            {!! $image = get_the_post_thumbnail(get_the_ID(), 'post-thumbnail', ['class' => 'rounded-3 card-img']) !!}
            @if($image)
                <div class="card-img-overlay d-flex align-items-start flex-column p-3">
                    <!-- Card overlay bottom -->
                    <div class="w-100 mt-auto">
                        <!-- Card category -->
                        {!! get_the_category_list() !!}
                    </div>
                </div>
            @endif

        </div>
        <div class="py-4 px-3">
            <h3 class="h5"><a href="@permalink" class="btn-link stretched-link text-reset">@title</a></h3>
            @if(! $image )
                <div class="position-relative z-10">
                    {!! get_the_category_list() !!}
                </div>
            @endif
            <div class="opacity-75">@excerpt</div>
            <!-- Card info -->
            <small class="nav-item small text-tertiary">{!!  get_the_date() !!}</small>
        </div>
    </div>
</article>
