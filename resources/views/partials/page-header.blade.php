<div class="page-header py-5 bg-tertiary mb-5 text-center">
    <div class="container">
        <h1 class="text-white mb-0">{!! $title !!}</h1>
    </div>
</div>
