{{--<header class="banner">--}}
{{--  <a class="brand" href="{{ home_url('/') }}">--}}
{{--    {!! $siteName !!}--}}
{{--  </a>--}}

{{--  @if (has_nav_menu('primary_navigation'))--}}
{{--    <nav class="nav-primary" aria-label="{{ wp_get_nav_menu_name('primary_navigation') }}">--}}
{{--      {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'echo' => false]) !!}--}}
{{--    </nav>--}}
{{--  @endif--}}
{{--</header>--}}


<nav class="navbar navbar-expand-lg navbar-light flex-column">
    <div class="navbar__section navbar__top">
        <div class="container">
            <a class="navbar-brand" href="{{ home_url('/') }}">
                <img src="@asset('images/match-better-logo.svg')" width="160px" class="ads-header" alt="">
            </a>
            <div>
                </small>
                <h5 class=" mb-0 text-tertiary">
                   <small> احصل على آخر الأخبار</small>
                <span class="text-primary">   <br> الرياضية التونسية من Match Better </span>
                </h5>
            </div>
{{--            <div class="ads-header">--}}
{{--                <a href="">--}}
{{--                    <img src="@asset('images/ads-970.jpg')" width="970px" class="ads-header" alt="">--}}
{{--                </a>--}}
{{--            </div>--}}
        </div>
    </div>

    <div class="navbar__section navbar__bottom">
        <div class="container align-items-lg-center">
            <a class="search-trigger ms-auto order-3 collapsed flex-shrink-0" data-bs-toggle="collapse" href="#searchBarCollapse" role="button" aria-expanded="false" aria-controls="searchBarCollapse">
                <i class="fas fa-search open"></i>
                <i class="fas fa-times close"></i>
            </a>
            <div class="navbar__social-links d-inline-block me-3 order-2">
{{--                <h6 class="text-white d-lg-none mt-3 opacity-50 small">تابع ماتش بتر على</h6>--}}
                <a href="https://www.facebook.com/matchbetter.tn" class="social-link" target="_blank"><i class="fab fa-facebook-f"></i></a>
{{--                <a href="#" class="social-link"><i class="fab fa-twitter"></i></a>--}}
{{--                <a href="#" class="social-link"><i class="fab fa-youtube"></i></a>--}}
            </div>
            <div class="col">
                <button class="navbar-toggler bg-white" type="button" data-bs-toggle="collapse" data-bs-target="#mainMenu" aria-controls="mainMenu" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon small"></span>
                </button>

                <div class="collapse navbar-collapse pt-3 pt-lg-0" id="mainMenu">
                    @if (has_nav_menu('primary_navigation'))
                        {!! wp_nav_menu($primaryMenuArgs) !!}
                    @endif
                </div>
            </div>


        </div>
    </div>
</nav>
<div class="collapse bg-light" id="searchBarCollapse">
    <div class="container">
        <div class=" card-body">
            {!! get_search_form(false) !!}
        </div>
    </div>
</div>


