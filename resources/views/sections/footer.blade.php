{{--<footer class="content-info">--}}
{{--  @php(dynamic_sidebar('sidebar-footer'))--}}
{{--</footer>--}}

<footer class="bg-dark text-white main-footer">
    <div class="text-primary bg-white mb-5">
        <svg xmlns="http://www.w3.org/2000/svg" height="40px" width="100%" viewBox="0 0 144.54 17.34" preserveAspectRatio="none" fill="#263238">
            <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
        </svg>
    </div>
    <div class="container">
        <div class="py-4">
            <div class="row">
                <div class="col-md-4 col-lg-3 mb-5">
                    <a class="navbar-brand" href="{{ home_url('/') }}">
                        <img src="@asset('images/match-better-logo.svg')" width="160px" class="ads-header" alt="">
                    </a>
                </div>
                <div class="col-md-8 col-lg-9 mb-4">
                    @if (has_nav_menu('primary_navigation'))
                        {!! wp_nav_menu($primaryMenuArgs) !!}
                    @endif
                </div>
            </div>

            <hr class="mb-3">
            جميع الحقوق محفوظة © 2022
        </div>
    </div>
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-auto me-5">--}}
{{--                <img src="{{ asset('images/ooredoo-logo.svg') }}" width="150px" alt="Ooredoo"> <br>--}}
{{--                <br>--}}
{{--                <small>© 2020 Ooredoo</small>--}}
{{--            </div>--}}
{{--            <div class="col-md">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-12 mb-4">--}}
{{--                        <h5 class="text-white">Contact</h5>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-4 mb-4">--}}
{{--                        <i class="fas fa-map-marker-alt"></i> Immeuble Zenith - 1053 Berge Du Lac <br>--}}
{{--                        Tunis, Tunisie--}}
{{--                    </div>--}}
{{--                    <div class="col-md-4 mb-4">--}}
{{--                        <i class="fas fa-phone"></i> 22 111 111--}}
{{--                    </div>--}}
{{--                    <div class="col-md-4 mb-4">--}}
{{--                        <i class="fas fa-envelope"></i> tounest3ich@ooredoo.tn--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}


{{--    </div>--}}
</footer>
