@extends('layouts.app')

@section('page-header')
    @include('partials.page-header')
@endsection
@section('content')
    <div class="container">
        @if (! have_posts())
            <x-alert type="warning" class=" p-3 rounded bg-light mb-4 ">
                عذرا، لم يتم العثور على نتائج
            </x-alert>
            <div class="w-50 mt-5">
                {!! get_search_form(false) !!}
            </div>
        @endif

        @while(have_posts()) @php(the_post())
        @include('partials.content-search')
        @endwhile

        <div class="mb-4 text-center">
            {!! get_the_posts_pagination([
                'prev_text'          => '<i class="fas fa-chevron-left"></i>',
                'next_text'          => '<i class="fas fa-chevron-right"></i>',
            ]) !!}
        </div>
    </div>

@endsection

@section('sidebar')
    @include('sections.sidebar')
@endsection
