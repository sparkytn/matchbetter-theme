<form role="search" method="get" class="search-form" action="{{ home_url('/') }}">
    <label class="mb-3 text-primary fw-bold">
      {{ _x('ابحث في هذا الموقع عن :', 'label', 'mb') }}

    </label>

    <div class="input-group">
        <input
            type="search"
            class="form-control"
            placeholder="{!! esc_attr_x('بحث عن  &hellip;',  'mb') !!}"
            value="{{ get_search_query() }}"
            name="s"
        >
        <button class="btn btn-primary"><i class="fas fa-search me-1 small"></i> {{ _x('بحث', 'mb') }}</button>
    </div>




</form>
