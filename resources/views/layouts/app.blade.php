<a class="sr-only focus:not-sr-only" href="#main">
    {{ __('Skip to content') }}
</a>

@include('sections.header')
@hasSection('page-header')
    @yield('page-header')
@endif
@hasSection('sidebar')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-9">
@endif
                <main id="main" class="main">
                    @yield('content')
                </main>
@hasSection('sidebar')
            </div>
@endif
@hasSection('sidebar')
        <div class="col-md-4 col-lg-3">
            <aside class="sidebar">
                @yield('sidebar')
            </aside>
        </div>
    </div>
</div>
@endif


@include('sections.footer')
